{
  description = "Geniality - Proof of concept";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { nixpkgs, utils, ... }:
    utils.lib.eachDefaultSystem (
      system:
        let
          pkgs = import nixpkgs {
            inherit system;
          };
        in
          rec {
            devShell = pkgs.mkShell {
              buildInputs = with pkgs; [
                yarn
                nodejs
                nodePackages."@angular/cli"
              ];
            };
          }
    );
}
