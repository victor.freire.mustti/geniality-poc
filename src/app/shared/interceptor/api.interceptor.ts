import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpResponse, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { Products } from '../mock/products';
import { Users } from '../mock/users';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;

        // wrap in delayed observable to simulate server api call
        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize()) // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .pipe(delay(500))
            .pipe(dematerialize());


        function handleRoute() {
            switch (true) {
                case url.endsWith('/login') && method === 'POST':
                    return login();
                case url.endsWith('/products') && method === 'GET':
                    return products();
                case url.match(/\/products\/\d+$/) && method === 'GET':
                    return getProductById();
                case url.endsWith('/users') && method === 'GET':
                    return users();
                case url.match(/\/users\/\d+$/) && method === 'GET':
                    return getUserById();
                // case url.endsWith('/users') && method === 'GET':
                //     return getUsers();
                // case url.match(/\/users\/\d+$/) && method === 'GET':
                //     return getUserById();
                // case url.match(/\/users\/\d+$/) && method === 'PUT':
                //     return updateUser();
                // case url.match(/\/users\/\d+$/) && method === 'DELETE':
                //     return deleteUser();
                default:
                    return next.handle(request);
            }
        }

        function login() {
            console.log(body)
            const { username, password } = body;

            const user = Users.find(u =>
                u.username == username && u.password == password);

            if (!user) return error('User not found.');

            return ok({ ...user });
        }

        function users() {
            return ok(Users);
        }

        function products() {
            return ok(Products);
        }

        function getProductById() {
            const product = Products.find(p => p.id === idFromUrl());
            return ok(product);
        }

        function getUserById() {
            const user = Users.find(u => u.id === idFromUrl());
            return ok(user);
        }

        function error(message: string) {
            return throwError({ error: { message } });
        }

        function ok(body?: any) {
            return of(new HttpResponse({ status: 200, body }))
        }

        function idFromUrl() {
            const urlParts = url.split('/');
            return parseInt(urlParts[urlParts.length - 1]);
        }
    }
}