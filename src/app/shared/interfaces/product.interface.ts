export default interface Product {
    id: number
    name: string
    description: string
    active: boolean
    imageURL: string
}