import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import User from '../../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  url = `${environment.server}/api/users`;

  constructor(
    private http: HttpClient
  ) { }

  getAll = () =>
    this.http.get<User[]>(this.url);

  get = (id: number) =>
    this.http.get<User>(`${this.url}/${id}`);

  delete = (id: number) =>
    this.http.delete(`${this.url}/${id}`);

  update = (id: number, value: User) =>
    this.http.put(`${this.url}/${id}`, value);

  add = (value: User) =>
    this.http.post(`${this.url}`, value);
}
