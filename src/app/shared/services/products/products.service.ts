import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import Product from '../../interfaces/product.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  url = `${environment.server}/api/products`;

  constructor(
    private http: HttpClient
  ) { }

  getAll = () =>
    this.http.get<Product[]>(this.url);

  get = (id: number) =>
    this.http.get<Product>(`${this.url}/${id}`);

  delete = (id: number) =>
    this.http.delete(`${this.url}/${id}`);

  update = (id: number, value: Product) =>
    this.http.put(`${this.url}/${id}`, value);

  add = (value: Product) =>
    this.http.post(`${this.url}`, value);
}
