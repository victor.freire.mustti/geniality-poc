import User from "src/app/shared/interfaces/user.interface";

export const Users: User[] = [
    {
        id: 1,
        username: "admin",
        password: "admin",
        createdAt: new Date().toISOString().toString(),
    },
    {
        id: 2,
        username: "user",
        password: "user",
        createdAt: new Date().toISOString().toString(),
    }
];