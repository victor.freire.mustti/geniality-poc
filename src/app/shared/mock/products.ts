import Product from "src/app/shared/interfaces/product.interface";

export const Products: Product[] = [
    {
        id: 1,
        name: "Example",
        description: "Long description of our example",
        active: true,
        imageURL: "https://picsum.photos/200/300",
    },
    {
        id: 2,
        name: "Example 2",
        description: "Long description of our example 2",
        active: true,
        imageURL: "https://picsum.photos/200/300",
    },
]