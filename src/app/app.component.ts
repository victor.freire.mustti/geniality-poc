import { Component } from '@angular/core';
import { PoMenuItem, PoToolbarAction } from '@po-ui/ng-components';
import User from './shared/interfaces/user.interface';
import { AccountService } from './shared/services/account/account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  user!: User | null;

  title = 'geniality-poc';
  toolbarTitle = "Geniality Proof of Concept";

  toolbarActions: PoToolbarAction[] = [
    {
      label: "Logout", type: "danger", separator: true, action: () => {
        this.accountService.logout();
      }
    }
  ];

  menus: PoMenuItem[] = [
    { label: "Home", shortLabel: "Home", link: "/" },
    { label: "Users", shortLabel: "Users", link: "/users" },
    { label: "Products", shortLabel: "Products", link: "/products" },
  ];

  constructor(private accountService: AccountService) {
    this.accountService.user.subscribe(x => this.user = x);
  }

  logout() {
    this.accountService.logout();
  }
}
