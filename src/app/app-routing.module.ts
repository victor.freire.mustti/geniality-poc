import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './core/home/home.component';
import { LoginComponent } from './core/login/login.component';
import { AuthGuard } from './shared/guards/auth.guard';

const productsModule = () => import('./core/products/products.module')
  .then(m => m.ProductsModule)

const usersModule = () => import('./core/users/users.module')
  .then(m => m.UsersModule)

const routes: Routes = [
  { path: 'products', loadChildren: productsModule, canActivate: [AuthGuard] },
  { path: 'users', loadChildren: usersModule, canActivate: [AuthGuard] },
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
