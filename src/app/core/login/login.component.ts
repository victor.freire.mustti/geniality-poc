import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PoPageLogin, PoPageLoginAuthenticationType } from '@po-ui/ng-templates';
import { AccountService } from 'src/app/shared/services/account/account.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  url = `${environment.server}/login`;
  authType = PoPageLoginAuthenticationType.Bearer;

  constructor(
    private accountService: AccountService,
    private router: Router,
  ) {
    if (this.accountService.userValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
  }

  submit(event: PoPageLogin) {
    this.accountService.login(event.login, event.password)
      .subscribe(data => {
        this.router.navigate(['/']);
      });
  }

}
