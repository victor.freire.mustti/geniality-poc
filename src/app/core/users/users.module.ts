import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UsersEditComponent } from './users-edit/users-edit.component';
import { PoDialogModule, PoDynamicModule, PoLoadingModule, PoPageModule, PoTableModule } from '@po-ui/ng-components';


@NgModule({
  declarations: [
    UsersComponent,
    UsersEditComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    PoPageModule,
    PoTableModule,
    PoDialogModule,
    PoDynamicModule,
    PoLoadingModule,
  ]
})
export class UsersModule { }
