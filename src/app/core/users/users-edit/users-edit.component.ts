import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PoDialogService, PoPageAction } from '@po-ui/ng-components';
import { PoDynamicField } from '@po-ui/ng-components/lib/components/po-dynamic/po-dynamic-field.interface';
import User from 'src/app/shared/interfaces/user.interface';
import { UsersService } from 'src/app/shared/services/users/users.service';

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.css']
})
export class UsersEditComponent implements OnInit {
  userId: string | number | null;
  isLoading: boolean;
  pageTitle = "";

  pageActions: PoPageAction[] = [
    { label: "Save", action: () => { } },
    {
      label: "Back", action: () => {
        this.dialog.confirm({
          title: "Are you sure?",
          message: "You are about to leave this page.\
                    All unsaved changes will be lost.",
          confirm: () => {
            this.router.navigate(['users']);
          }
        })
      }
    }
  ];

  fields: PoDynamicField[] = [
    { label: "Username", property: "username" },
    { label: "Password", property: "password" },
  ];

  form!: NgForm;
  user!: User;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialog: PoDialogService,
    private usersService: UsersService,
  ) {
    this.isLoading = true;
    this.userId = this.route.snapshot.paramMap.get('id');
    if (this.userId == "new") {
      this.pageTitle = "New User";
      this.isLoading = false;
    } else {
      this.pageTitle = "Edit User";
      this.usersService.get(Number(this.userId))
        .subscribe(data => {
          this.isLoading = false;
          this.user = data;
        })
    }
  }

  ngOnInit(): void {
  }

  getForm(form: NgForm) {
    this.form = form;
  }
}
