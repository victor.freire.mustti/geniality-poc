import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PoPageAction, PoTableAction, PoTableColumn } from '@po-ui/ng-components';
import User from 'src/app/shared/interfaces/user.interface';
import { UsersService } from 'src/app/shared/services/users/users.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  pageTitle = "Users";
  isLoading: boolean;

  actions: PoPageAction[] = [
    { label: "New", action: () => { this.router.navigate(['users', 'new']) } },
  ];

  tableColumns: PoTableColumn[] = [
    { label: "ID", property: "id", type: "number" },
    { label: "Username", property: "username", type: "string" },
    { label: "Created At", property: "createdAt", type: "dateTime" },
  ];

  tableActions: PoTableAction[] = [
    {
      label: "Edit",
      action: this.editUser.bind(this)
    },
    {
      label: "Remove",
      type: "danger",
      separator: true,
    }
  ];

  tableItems!: User[];

  constructor(
    private router: Router,
    private usersService: UsersService
  ) {
    this.isLoading = true;
    this.usersService.getAll()
      .subscribe(data => {
        this.tableItems = data;
        console.log(data)
        this.isLoading = false;
      })
  }

  ngOnInit(): void {
  }

  editUser(item: User) {
    this.router.navigate(['users', `${item.id}`]);
  }
}
