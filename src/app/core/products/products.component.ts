import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PoPageAction, PoTableAction, PoTableColumn } from '@po-ui/ng-components';
import Product from 'src/app/shared/interfaces/product.interface';
import { ProductsService } from 'src/app/shared/services/products/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  pageTitle = "Products";
  isLoading: boolean;

  actions: PoPageAction[] = [
    { label: "New", action: () => { this.router.navigate(['products', 'new']) } },
  ];

  tableColumns: PoTableColumn[] = [
    { label: "ID", property: "id" },
    { label: "Name", property: "name" },
    { label: "Description", property: "description", width: '30px' },
    { label: "Active", property: "active" },
    { label: "ID", property: "id" },
    { label: "Name", property: "name" },
    { label: "Description", property: "description" },
    { label: "Active", property: "active" },
    { label: "ID", property: "id" },
    { label: "Name", property: "name" },
    { label: "Description", property: "description" },
    { label: "Active", property: "active" },
    { label: "ID", property: "id" },
    { label: "Name", property: "name" },
    { label: "Description", property: "description" },
    { label: "Active", property: "active" },
    { label: "ID", property: "id" },
    { label: "Name", property: "name" },
    { label: "Description", property: "description" },
    { label: "Active", property: "active" },
  ];

  tableItems!: Product[];

  tableActions: PoTableAction[] = [
    {
      label: "Edit",
      action: this.editProduct.bind(this)
    },
    {
      label: "Remove",
      separator: true,
      type: "danger",
    },
  ];

  constructor(
    private router: Router,
    private productsService: ProductsService
  ) {
    this.isLoading = true;

    this.productsService.getAll()
      .subscribe(data => {
        this.tableItems = data
        this.isLoading = false;
      })
  }

  editProduct(item: Product) {
    console.log(item);
    this.router.navigate(['products', `${item.id}`]);
  }

  ngOnInit(): void {

  }

}
