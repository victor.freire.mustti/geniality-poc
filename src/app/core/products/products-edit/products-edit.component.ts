import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PoDialogService, PoPageAction } from '@po-ui/ng-components';
import { PoDynamicField } from '@po-ui/ng-components/lib/components/po-dynamic/po-dynamic-field.interface';
import Product from 'src/app/shared/interfaces/product.interface';
import { ProductsService } from 'src/app/shared/services/products/products.service';

@Component({
  selector: 'app-products-edit',
  templateUrl: './products-edit.component.html',
  styleUrls: ['./products-edit.component.css']
})
export class ProductsEditComponent implements OnInit {
  pageTitle = "New";
  isLoading: boolean;
  productId: string | number | null;
  form!: NgForm;
  product!: Product;

  actions: PoPageAction[] = [
    {
      label: "Back", action: () => {
        this.dialog.confirm({
          title: "Are you sure?",
          message: "You are about to leave this page.\
                  All unsaved changes will be lost.",
          confirm: () => {
            this.router.navigate(['products']);
          }
        })
      }
    },
    { label: "Save", action: () => { } }
  ];

  formFields: PoDynamicField[] = [
    { label: "Name", property: "name", type: "string" },
    { label: "Description", property: "description", type: "string" },
    { label: "Active", property: "active", type: "boolean" },
    { label: "Image URL", property: "imageURL", type: "string" }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productsService: ProductsService,
    private dialog: PoDialogService
  ) {
    this.isLoading = true;

    this.productId = this.route.snapshot.paramMap.get('id');
    if (this.productId == "new") {
      this.pageTitle = "New Product";
      this.isLoading = false;
    } else {
      this.productId = Number(this.productId);
      this.pageTitle = "Edit Product";

      this.productsService.get(this.productId)
        .subscribe((data: any) => {
          this.isLoading = false;
          this.product = data;
        })
    }
  }

  getForm(form: NgForm) {
    this.form = form;
  }

  ngOnInit(): void {
  }

  saveProduct() {
    this.productsService.add(this.form.value)
  }

}
