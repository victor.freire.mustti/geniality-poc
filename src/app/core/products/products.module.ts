import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { PoDialogModule, PoDynamicModule, PoLoadingModule, PoPageModule, PoTableModule, PoWidgetModule } from '@po-ui/ng-components';
import { ProductsEditComponent } from './products-edit/products-edit.component';


@NgModule({
  declarations: [
    ProductsComponent,
    ProductsEditComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    PoPageModule,
    PoTableModule,
    PoDynamicModule,
    PoLoadingModule,
    PoDialogModule,
    PoWidgetModule
  ]
})
export class ProductsModule { }
